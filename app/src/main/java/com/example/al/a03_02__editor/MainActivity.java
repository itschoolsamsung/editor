package com.example.al.a03_02__editor;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class MainActivity extends Activity implements View.OnClickListener {

    EditText editText;
    EditText editTextDir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = (EditText)findViewById(R.id.editText);
        editTextDir = (EditText)findViewById(R.id.editTextTextDir);
        findViewById(R.id.button_load).setOnClickListener(this);
        findViewById(R.id.button_save).setOnClickListener(this);
        findViewById(R.id.button_load_dialog).setOnClickListener(this);
    }

    private boolean loadText(String fileName) {
        FileReader fileR;
        try {
            fileR = new FileReader(fileName);
            int c;
            StringBuffer s = new StringBuffer();
            while ((c = fileR.read()) != -1) {
                s.append((char)c);
            }
            editText.setText(s);
            return true;
        } catch (IOException e) {
            editText.setText(e.toString());
        }
        return false;
    }

    private boolean saveText(String fileName) {
        FileWriter fileW;
        try {
            fileW = new FileWriter(fileName);
            fileW.write(editText.getText().toString());
            fileW.flush();
            return true;
        } catch (IOException e) {
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        String dir = getFilesDir().toString();
        editTextDir.setText(dir);

        switch (v.getId()) {
            case R.id.button_load:
//                loadText(Environment.getExternalStorageDirectory().toString() + "/test");
                loadText(dir + "/test");
                break;
            case R.id.button_save:
//                if (saveText(Environment.getExternalStorageDirectory().toString() + "/test")) {
                if (saveText(dir + "/test")) {
                    Toast.makeText(getApplicationContext(), "Saved!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.button_load_dialog:
                Intent intent = new Intent().setType("file/*").setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, 123);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==123 && resultCode==RESULT_OK) {
            Uri fileUri = data.getData();
            if (fileUri != null) {
                loadText(fileUri.getPath());
            }
        }
    }

}
